from .base import BaseVAE
from .dfcvae import DFCVAE
from .beta_vae import BetaVAE
from .betatc_vae import BetaTCVAE
from .cat_vae import CategoricalVAE
from .dip_vae import DIPVAE
from .fvae import FactorVAE # not working
from .gamma_vae import GammaVAE # not working
from .hvae import HVAE # not working
from .info_vae import InfoVAE #not working
from .iwae import IWAE # not working
from .joint_vae import JointVAE
from .logcosh_vae import LogCoshVAE
#from .lvae import EncoderBlock, LadderBlock, LVAE
from .miwae import MIWAE
from .mssim_vae import MSSIMVAE
#from .swae import SWAE
#from .twostage_vae import TwoStageVAE
#from .vampvae import VampVAE
#from .vanilla_vae import VanillaVAE
#from .vq_vae import VectorQuantizer
#from .wae_mmd import WAE_MMD
vae_models = {
    "DFCVAE": DFCVAE,
    "Base": BaseVAE,
    "BetaVAE": BetaVAE,
    "BetaTCVAE" : BetaTCVAE,
    "CategoricalVAE" : CategoricalVAE,
    "DIPVAE" : DIPVAE,
    "FactorVAE" : FactorVAE,
    "GammaVAE" : GammaVAE,
    "HVAE" : HVAE,
    "InfoVAE" : InfoVAE,
    "IWAE" : IWAE,
    "JointVAE" : JointVAE,
    "LogCoshVAE" : LogCoshVAE,
    "MIWAE" : MIWAE,
    "MSSIMVAE" : MSSIMVAE,
   # "SWAE" : SWAE,
   # "TwoStageVAE" : TwoStageVAE,
   # "VampVAE" : VampVAE,
   # "VanillaVAE" : VanillaVAE,
    #"VectorQuantizer" : VectorQuantizer,
    #"WAE_MMD" : WAE_MMD,  
}

__all__ = [cls.__name__ for cls in vae_models.values()] 
