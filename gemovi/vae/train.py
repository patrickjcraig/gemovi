import argparse
import os
import sys
from pathlib import Path

import yaml
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import (
    LearningRateMonitor,
    ModelCheckpoint,
    EarlyStopping,
)
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.strategies.ddp import DDPStrategy
from pytorch_lightning.utilities.seed import seed_everything

from ..common.utils import get_config
from .dataset import VAEDataset
from .experiment import VAEXperiment
from .models import vae_models


def main():
    parser = argparse.ArgumentParser(description="Generic runner for VAE models")
    parser.add_argument(
        "--config",
        "-c",
        dest="filename",
        metavar="FILE",
        help="path to the config file",
        default="model_defaults.yaml",
    )

    args = parser.parse_args()
    config = get_config(args.filename)

    train_params = config["trainer_params"]

    name = config["vae_model_params"]["name"]
    fmt_log_dir = train_params.pop("log_dir").format(framework="lightning")
    tb_logger = TensorBoardLogger(name=name, save_dir=fmt_log_dir)
    tb_logger.log_hyperparams(config)

    ckpt_path = train_params.pop("ckpt_path")

    # For reproducibility
    seed_everything(config["exp_params"]["manual_seed"], True)

    model = vae_models[name](**config["vae_model_params"])
    experiment = VAEXperiment(model, config["exp_params"])

    data = VAEDataset(
        **config["data_params"],
        pin_memory=len(train_params["devices"]) != 0,
    )

    strat = {}
    if sys.platform != "win32":
        strat["strategy"] = DDPStrategy(find_unused_parameters=False)

    data.setup()
    runner = Trainer(
        logger=tb_logger,
        callbacks=[
            LearningRateMonitor(),
            ModelCheckpoint(
                save_top_k=train_params["max_epochs"],
                dirpath=os.path.join(tb_logger.log_dir, "checkpoints"),
                monitor="val_loss",
                save_last=True,
                every_n_epochs=1,
            ),
            EarlyStopping(monitor="val_loss", patience=15),
        ],
        **strat,
        **train_params,
    )

    Path(f"{tb_logger.log_dir}/Samples").mkdir(exist_ok=True, parents=True)
    Path(f"{tb_logger.log_dir}/Reconstructions").mkdir(exist_ok=True, parents=True)

    print(f"======= Training {config['vae_model_params']['name']} =======")
    runner.fit(experiment, datamodule=data, ckpt_path=ckpt_path)


if __name__ == "__main__":
    main()
