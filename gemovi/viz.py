from __future__ import annotations

import base64
import functools
import os
from dataclasses import dataclass, fields
from io import BytesIO
from pathlib import Path
from typing import Any, Callable, List, Optional, Sequence, Type

import numpy as np
import pandas as pd
import pyqtgraph as pg
import torch
from PIL import Image
from pyqtgraph.Qt import QtCore, QtWidgets
from scipy import stats
from sklearn.cluster import FeatureAgglomeration
from sklearn.decomposition import PCA
from sklearn.random_projection import GaussianRandomProjection
from utilitys import (
    AtomicProcess,
    NestedProcess,
    ParamEditor,
    PrjParam,
    RunOpts,
    fns,
    widgets,
)

from .common.constants import get_device
from .common.dataset import default_image_transforms
from .common.utils import (
    get_config,
    load_trainer_state_dict,
    pad_image_to_size,
    tensor_to_np,
    to_pil_image,
)
from .dcgan.models import GAN
from .vae.models import DFCVAE, BaseVAE

device = get_device(1)

# Ensure reruns are determinisic
torch.manual_seed(0)
pg.setConfigOptions(imageAxisOrder="row-major")


def NoneTransformer(*args, **kwargs):
    return None


transformer_classes = {
    "PCA": PCA,
    "Gaussian Projecton": GaussianRandomProjection,
    "Feature Agglomeration": lambda *args, **kwargs: FeatureAgglomeration(),
    "None": NoneTransformer,
}


def allow_none_xform_and_tensor_data(func):
    def wrapper(X, **kwargs) -> torch.Tensor:
        if func is None:
            return X
        if isinstance(X, torch.Tensor):
            X = tensor_to_np(X)
        out_kwargs = {}
        for k, v in kwargs.items():
            if isinstance(v, torch.Tensor):
                v = tensor_to_np(v)
            out_kwargs[k] = v

        out = func(X, **out_kwargs)
        if isinstance(out, np.ndarray):
            out = torch.from_numpy(out).to(device)
        return out

    return wrapper


def _xycallable(X: torch.Tensor, *, y: torch.Tensor = None) -> torch.Tensor:
    ...


def _xcallable(X: torch.Tensor) -> torch.Tensor:
    ...


class NpOrNoneTransformer:
    wrapped_traits = ["transform", "inverse_transform", "fit", "fit_transform"]
    transform = _xcallable
    inverse_transform = _xcallable
    fit = _xycallable
    fit_transform = _xycallable

    def __init__(self, tformer=None):
        self.tformer = None
        self.set_tformer(tformer)

    def set_tformer(self, tformer=None):
        self.tformer = tformer
        for trait in self.wrapped_traits:
            to_wrap = getattr(self.tformer, trait) if self.tformer else None
            wrapped = allow_none_xform_and_tensor_data(to_wrap)
            setattr(self, trait, wrapped)


class NamedImageItem(pg.ImageItem):
    sig_clicked = QtCore.Signal(object)

    def __init__(self, name=None):
        super().__init__()
        # Update anchor to center the label above the image
        self.name_label = pg.TextItem(
            text=name, color=(255, 255, 255, 255), anchor=(0.5, 1)
        )

    def addToParent(self, parent):
        parent.addItem(self)
        parent.addItem(self.name_label)

    def centerLabel(self):
        center_pos = self.boundingRect().center()
        self.name_label.setPos(center_pos.x(), 0)

    def mouseClickEvent(self, ev):
        self.sig_clicked.emit(self)
        super().mouseClickEvent(ev)


def read_image_folder(folder_or_file_list, ext="*"):
    images, names = [], []
    if os.path.isfile(folder_or_file_list):
        folder_or_file_list = [Path(folder_or_file_list)]
    else:
        folder_or_file_list = fns.naturalSorted(
            Path(folder_or_file_list).glob(f"*.{ext}")
        )
    for file in folder_or_file_list:
        try:
            images.append(Image.open(file))
        except IOError:
            continue
        names.append(file.name)
    return images, names


@dataclass
class TooltipData:
    name: str = None
    label: str = None
    image_data: bytes | Any = None
    image_read_func: Callable[[Any], Image.Image] = to_pil_image

    def __str__(self):
        label_str = f"Label: {self.label}<br/>" if self.label else ""
        name_str = self.name or "<None>"
        html = (
            f'<div style="text-align:center">'
            f"Name: {name_str}<br/>"
            f"{label_str}"
            f"</div>"
        )
        if self.image_data is not None:
            img = self.image_data_to_b64().decode()
            html += f'<img src="data:image/png;base64,{img}"/>'
        return html

    def image_data_to_b64(self, size: int | tuple = 64):
        image_data = self.image_data
        if image_data is None or isinstance(image_data, bytes):
            # Already converted / doesn't exist, nothing to replace
            return image_data
        # Lazy evaluation in all other cases
        image_data = self.image_read_func(image_data)
        image_data = pad_image_to_size(image_data, size)
        buff = BytesIO()
        image_data.save(buff, format="jpeg", quality=80)
        image_bytes = base64.b64encode(buff.getvalue())
        self.image_data = image_bytes
        return image_bytes


@dataclass
class SampleInfo:
    samples: torch.Tensor = None
    numeric_labels: Optional[np.ndarray] = None
    number_to_label_map: pd.Series = None
    colormap: str = "plasma"

    def unset_labels(self):
        self.numeric_labels = None
        self.number_to_label_map = pd.Series({1.0: None})

    def __post_init__(self):
        self.unset_labels()

    def state_dict(self):
        return {field.name: getattr(self, field.name) for field in fields(self)}

    def load_state_dict(self, state_dict):
        found = set()
        for k, v in state_dict.items():
            if hasattr(self, k):
                setattr(self, k, v)
                found.add(k)
        msg = ""
        self_keys = set(self.state_dict())
        missing_keys = self_keys - found
        extra_keys = found - self_keys
        if not missing_keys and not extra_keys:
            msg = "<All sample keys loaded successfully>"
        if missing_keys:
            msg += f"<Missing sample keys: {missing_keys}>"
        if extra_keys:
            msg += f"<Extra sample keys: {extra_keys}>"
        return msg


class PreviewItemsMixin:
    sig_preview_clicked = QtCore.Signal(object, int)
    """Item clicked and it's raveled preview index"""

    def __init__(self):
        view = pg.GraphicsView()
        layout = pg.GraphicsLayout()
        view.setCentralItem(layout)
        self.popouts = {}
        self.selected_preview_idx = 0
        self.preview_widget, self.preview_layout = view, layout
        self.preview_items: list[list[NamedImageItem]] = [[]]
        self.num_preview_rows, self.num_preview_cols = 0, 0

        old_resize = view.resizeEvent

        def new_resize(event):
            old_resize(event)
            self.trigger_autorange()

        view.resizeEvent = new_resize

    def update_previews(
        self, images, confidences=None, names: List[str] | str = "", show_labels=False
    ):
        preview_items = self.preview_items
        if not names:
            names = [""] * len(images)
        else:
            names = [str(name) + ": " for name in names]
        if not confidences:
            confidences = [None] * len(images)
        for ii in range(len(preview_items)):
            for jj in range(len(preview_items[ii])):
                ravel_idx = ii * len(preview_items[ii]) + jj
                if ravel_idx < len(images):
                    item = preview_items[ii][jj]
                    item.setImage(images[ravel_idx])
                    item.name_label.setText(
                        names[ravel_idx] + str(confidences[ravel_idx])
                    )
                    item.centerLabel()
                    item.name_label.setVisible(show_labels)
        self.trigger_autorange()

    def trigger_autorange(self):
        if len(self.preview_items):
            # Autoranging one will autorange all since they are linked
            self.preview_items[0][0].getViewBox().autoRange()

    def make_previews(self, num_rows=0, num_cols=0):
        self.num_preview_rows, self.num_preview_cols = num_rows, num_cols
        preview_items = [
            [NamedImageItem() for _ in range(num_cols)] for _ in range(num_rows)
        ]
        layout = self.preview_layout
        layout.clear()
        self.preview_items = preview_items
        referencevb = None
        for ii in range(num_rows):
            for jj in range(num_cols):
                proxy = layout.addViewBox(ii, jj, lockAspect=True)
                preview_items[ii][jj].addToParent(proxy)
                proxy.autoRange()
                proxy.invertY()
                preview_items[ii][jj].sig_clicked.connect(self.on_image_click)
                if referencevb:
                    proxy.setXLink(referencevb)
                    proxy.setYLink(referencevb)
                referencevb = referencevb or proxy

    def on_image_click(self, item):
        ravel_idx = 0
        for ii in range(self.num_preview_rows):
            for jj in range(self.num_preview_cols):
                if self.preview_items[ii][jj] is item:
                    self.sig_preview_clicked.emit(item, ravel_idx)
                    return
                ravel_idx += 1

    def popout_sample(self, sample_idx=0, stay_on_top=True):
        pw = pg.PlotWidget()
        item = pg.ImageItem()
        pw.addItem(item)
        row, col = np.unravel_index(
            sample_idx, (self.num_preview_rows, self.num_preview_cols)
        )
        ref_item = self.preview_items[row][col]

        def on_update():
            item.setImage(ref_item.image)

        ref_item.sigImageChanged.connect(on_update)
        if stay_on_top:
            # False positive
            # noinspection PyTypeChecker
            pw.setWindowFlags(QtCore.Qt.WindowType.WindowStaysOnTopHint)
        pw.show()
        pw.setAspectLocked(True)
        pw.invertY()
        self.popouts[sample_idx] = pw

    def update_selected_preview(
        self, preview_idx: int = None, image_data: np.ndarray = None
    ):
        """
        If the selected sample is already present in the previews, highlight it.
        Otherwise, evict the oldest sample and add the new one before highlighting.
        :param preview_idx:
            value: 0
        :param image_data: If not None, replaces the image data in the preview
        """
        if preview_idx == self.selected_preview_idx and image_data is None:
            return
        nrows, ncols = self.num_preview_rows, self.num_preview_cols

        old_sample_idx = self.selected_preview_idx
        if old_sample_idx is not None:
            oldrow, oldcol = np.unravel_index(old_sample_idx, (nrows, ncols))
            self.preview_items[oldrow][oldcol].setBorder(None)
        self.selected_preview_idx = preview_idx

        if preview_idx is None:
            return

        newrow, newcol = np.unravel_index(preview_idx, (nrows, ncols))
        self.preview_items[newrow][newcol].setBorder(pg.mkPen("r", width=5))
        if image_data is not None:
            self.preview_items[newrow][newcol].setImage(image_data)
        self.preview_widget.update()


class ModelTab(QtWidgets.QWidget, PreviewItemsMixin):
    def __init__(self, model):
        QtWidgets.QWidget.__init__(self)
        PreviewItemsMixin.__init__(self)
        self.editor = ParamEditor()
        self.model = model
        self.editor.applyBtn.hide()

        contents = self.setup_gui()
        if contents is not None:
            layout = QtWidgets.QVBoxLayout()
            self.setLayout(layout)
            layout.addWidget(contents)
        self.post_init(self.editor)

    def on_model_change(self):
        pass

    def setup_gui(self) -> Optional[QtWidgets.QWidget]:
        raise NotImplementedError

    def post_init(self, pe: ParamEditor):
        pass

    def forward(self, input):
        raise NotImplementedError

    def get_image_size(self):
        raise NotImplementedError

    def get_num_latent_dims(self):
        raise NotImplementedError

    def image_as_normed_tensor(self, image, add_batch_dim=True):
        tforms = default_image_transforms(self.get_image_size())
        if isinstance(image, torch.Tensor):
            norm = tforms.transforms[-1]
            return norm(image)
        # else
        image = to_pil_image(image)
        image = tforms(image).to(device)
        if add_batch_dim:
            image = image.unsqueeze(0)
        return image


class GANGenerativeTab(ModelTab):
    mapping_granularity = 0.05

    def __init__(self, model):
        self.xdim, self.ydim = 0, 1
        self.eviction_idx = 0

        self.dim_tformer = NpOrNoneTransformer()

        self.last_pos = (0, 0)
        self.sample_idx_cache = []
        self.eviction_idx = 0

        self.samples_info = SampleInfo()

        self.tgt = pg.TargetItem((0, 0))
        self.gaus_overlay_item = pg.ImageItem()
        self.plot_widget = pg.PlotWidget()
        self.legend = pg.LegendItem()
        self.scatterplot = pg.ScatterPlotItem(brush=None, pen="w", hoverable=True)

        super().__init__(model)

    @property
    def samples(self):
        return self.samples_info.samples

    @property
    def numeric_labels(self):
        labels = self.samples_info.numeric_labels
        if labels is None:
            labels = np.ones(len(self.samples), dtype="float32")
        return labels

    def transformed_samples(self, sample_idxs: int | Sequence[int]):
        flatten = np.isscalar(sample_idxs)
        if flatten:
            sample_idxs = [sample_idxs]
        xformed = self.dim_tformer.transform(self.samples_info.samples[sample_idxs])
        if flatten:
            return xformed[0]
        return xformed

    def on_model_change(self):
        self.reset_sample_latents()

    def post_init(self, pe):
        preview_proc = pe.registerFunc(
            self.make_previews, preview_widget=dict(ignore=True)
        )
        preview_proc(num_rows=4, num_cols=4, preview_widget=self.preview_widget)

        self.reset_sample_latents()
        self.tgt.sigPositionChanged.connect(self.on_tgt_move)
        self.update_dims_proc = pe.registerFunc(
            self.update_dims, runOpts=RunOpts.ON_CHANGED
        )
        pe.registerFunc(
            self.set_transformer,
            transformer=dict(limits=list(transformer_classes), type="list"),
            n_components=dict(value=self.get_num_latent_dims(), type="int"),
        )
        pe.registerFunc(self.reset_sample_latents)
        pe.registerFunc(self.create_uniform_map)
        pe.registerFunc(self.create_perturbation_map)
        pe.registerFunc(self.popout_sample)
        pe.registerFunc(
            self.plot_widget.getViewBox().setAspectLocked,
            runOpts=RunOpts.ON_CHANGED,
        )
        pe.registerFunc(self.save_samples)
        pe.registerFunc(self.load_samples)
        pe.registerFunc(self.update_granularity, runOpts=RunOpts.ON_CHANGED)

        self.on_tgt_move(self.tgt)
        self.plot_widget.setAspectLocked(True)

        self.scatterplot.sigHovered.connect(self.on_scatter_hover)
        self.scatterplot.sigClicked.connect(self.on_scatter_click)
        self.sig_preview_clicked.connect(self.on_preview_clicked)

        pe.treeBtnsWidget.show()
        pe.collapseAllBtn.click()

    def make_previews(self, num_rows=0, num_cols=0):
        super().make_previews(num_rows, num_cols)
        new_cache = list(range(num_rows * num_cols))
        setter_stop = min(len(self.sample_idx_cache), len(new_cache))
        new_cache[:setter_stop] = self.sample_idx_cache[:setter_stop]
        self.sample_idx_cache = new_cache
        if self.samples is not None and len(self.samples) > max(self.sample_idx_cache):
            self.update_previews()
        self.update_selected_preview(0)
        self.on_tgt_move(self.tgt)

    def get_num_latent_dims(self):
        return self.model.opts.num_latent_dims

    @functools.lru_cache(maxsize=1)
    def get_image_size(self):
        noise = torch.zeros(1, self.get_num_latent_dims(), device=device)
        return self.forward(noise).shape[-2:]

    def forward(self, input: torch.Tensor):
        return self.model.generator(input.view(*input.shape, 1, 1))

    def setup_gui(self):
        pw = self.plot_widget

        ti = self.tgt
        ti.setZValue(100)
        pw.addItem(ti)

        self.gaus_overlay_item.setOpacity(0.75)
        self.gaus_overlay_item.setColorMap("viridis")
        self.create_uniform_map()
        pw.addItem(self.gaus_overlay_item)

        self.plot_widget.addItem(self.scatterplot)
        self.legend.setParentItem(self.plot_widget.plotItem)

        contents = widgets.EasyWidget.buildWidget(
            [
                widgets.EasyWidget(
                    [pw, self.editor.dockContentsWidget], useSplitter=True
                ),
                self.preview_widget,
            ],
            useSplitter=True,
        )
        return contents

    def update_selected_sample(
        self, sample_idx: int = None, image_data: np.ndarray = None
    ):
        """
        Update the selected sample in the preview widget.
        :param sample_idx:
            type: int
            value: 0
        :param image_data:
            ignore: True
        """
        if sample_idx is None:
            self.update_selected_preview()
            return
        try:
            preview_idx = self.sample_idx_cache.index(sample_idx)
        except ValueError:
            nrows, ncols = self.num_preview_rows, self.num_preview_cols
            preview_idx = self.eviction_idx
            self.eviction_idx = (self.eviction_idx + 1) % (nrows * ncols)
            self.sample_idx_cache[preview_idx] = sample_idx
        self.update_selected_preview(preview_idx, image_data)
        xformed = self.transformed_samples(sample_idx)
        self.tgt.setPos(xformed[self.xdim], xformed[self.ydim])

    def on_scatter_hover(self, item, points, _evt):
        if not len(points):
            item.setToolTip(None)
            return
        tooltip: TooltipData = points[0].data()
        if tooltip.label is None:
            numeric_lbl = self.numeric_labels[points[0].index()]
            tooltip.label = self.samples_info.number_to_label_map[numeric_lbl]
        item.setToolTip(str(tooltip))

    def on_scatter_click(self, _item, points, _evt):
        if not len(points):
            self.tgt.hide()
            self.update_selected_preview()
        self.tgt.show()
        idx = points[0].index()
        self.update_selected_sample(idx)

    def on_preview_clicked(self, item, ravel_idx):
        self.update_selected_sample(self.sample_idx_cache[ravel_idx])

    def get_random_latents(self, n_samples=None):
        if n_samples is None:
            if self.samples is None:
                n_samples = self.num_preview_rows * self.num_preview_cols
            else:
                n_samples = self.samples.shape[0]
        return torch.randn(n_samples, self.get_num_latent_dims(), device=device)

    def update_dims(self, xdim=0, ydim=1):
        if self.samples is None or not len(self.samples):
            return
        dims = np.array([xdim, ydim])
        dummy_data = self.transformed_samples(0)
        dims = np.clip(dims, 0, len(dummy_data) - 1)
        self.xdim, self.ydim = dims
        self.plot_samples(user_data=self.scatterplot.data["data"])

    def reset_sample_latents(self, n_samples=None):
        self.samples_info.samples = self.get_random_latents(n_samples)
        self.samples_info.unset_labels()
        self.plot_samples()

        self.update_previews()

    def update_previews(self, image_data=None, *args, **kwargs):
        if image_data is None:
            vis_samples = self.samples[self.sample_idx_cache]
            image_data = tensor_to_np(self.forward(vis_samples)).transpose((0, 2, 3, 1))
        super().update_previews(image_data, *args, **kwargs)
        if self.selected_preview_idx is not None:
            sample_idx = self.sample_idx_cache[self.selected_preview_idx]
            xformed = self.transformed_samples(sample_idx)
            self.tgt.setPos(xformed[self.xdim], xformed[self.ydim])

    def on_tgt_move(self, tgt: pg.TargetItem):
        if self.samples is None:
            return
        x, y = tgt.pos()
        sample_idx = self.sample_idx_cache[self.selected_preview_idx]
        xformed = self.transformed_samples([sample_idx])
        xformed[0, self.xdim] = x
        xformed[0, self.ydim] = y

        selected_sample = self.dim_tformer.inverse_transform(xformed).type_as(
            self.samples
        )
        pred = self.forward(selected_sample)
        # confidences = self.model.discriminator(pred).view(-1).detach().cpu().numpy()
        pred_imgs = tensor_to_np(pred).transpose((0, 2, 3, 1))

        self.update_selected_preview(self.selected_preview_idx, pred_imgs[0])

        old_data = self.scatterplot.data
        self.samples[[sample_idx]] = selected_sample
        for key, val in zip(["x", "y"], (x, y)):
            old_data[key][sample_idx] = val
        self.scatterplot.updateSpots(old_data)

    def update_granularity(self, granularity=0.05):
        self.mapping_granularity = granularity

    def create_perturbation_map(self, num_samples=100, std_devs=3):
        """
        Creates a map of how strongly minor changes in the currently viewed dimensions
        affect the generated image.
        """
        samples = (
            torch.rand(num_samples, self.get_num_latent_dims(), device=device)
            * std_devs
            - std_devs / 2
        )
        base_pred = self.forward(samples).detach()
        check_space = torch.linspace(
            -1, 1, int(1 / self.mapping_granularity), device=device
        )
        dy, dx = torch.meshgrid(check_space, check_space, indexing="ij")
        dx_vec = dx.reshape(-1)
        dy_vec = dy.reshape(-1)
        map_xy = np.arange(-std_devs, std_devs, self.mapping_granularity)
        out_map = np.zeros((len(map_xy), len(map_xy)), dtype=np.float32)
        for ii, sample in enumerate(samples):
            # TODO: Tile somehow to get shapes broadcastable without inplace update
            batch = sample.unsqueeze(0).expand(dx_vec.size(0), *sample.shape)
            # Clone for in-place operations
            batch = batch.clone()
            batch[:, self.xdim] += dx_vec
            batch[:, self.ydim] += dy_vec
            pred = self.forward(batch).detach()
            ref = base_pred[ii]
            diff = torch.abs(pred - ref).view(len(dx_vec), -1).mean(dim=1).cpu().numpy()
            # Find out which output locations should be updated
            xy_batch = batch[:, [self.xdim, self.ydim]].cpu().numpy().reshape(-1, 2)
            x_idxs = np.searchsorted(map_xy, xy_batch[:, 0])
            y_idxs = np.searchsorted(map_xy, xy_batch[:, 1])
            # f = interpolate.interp2d(xy_batch[:, 0], xy_batch[:, 1], diff, kind="cubic")
            x_interp = np.interp(map_xy[x_idxs], xy_batch[:, 0], diff)
            y_interp = np.interp(map_xy[y_idxs], xy_batch[:, 1], diff)
            out_map[y_idxs, x_idxs] += (x_interp + y_interp) / 2
        self.gaus_overlay_item.setImage(
            out_map, rect=(-std_devs, -std_devs, std_devs * 2, std_devs * 2)
        )

    def create_uniform_map(self, std_devs=5):
        gran = self.mapping_granularity
        yy, xx = np.mgrid[-std_devs:std_devs:gran, -std_devs:std_devs:gran]
        gaus_data = stats.norm.pdf(yy) * stats.norm.pdf(xx)
        self.gaus_overlay_item.setImage(
            gaus_data, rect=(-std_devs, -std_devs, std_devs * 2, std_devs * 2)
        )

    def load_samples(self, file="./sample_info.pt"):
        """
        :param file:
        type=file
        """
        self.samples_info.load_state_dict(torch.load(file))
        self.tgt.setPos(
            self.samples[self.selected_preview_idx, self.xdim],
            self.samples[self.selected_preview_idx, self.ydim],
        )
        self.plot_samples()

    def save_samples(self, file="./sample_info.pt"):
        """
        :param file:
        type=file
        """
        torch.save(self.samples_info.state_dict(), file)

    def plot_samples(self, *, size=0.15, user_data=None, show_legend=False):
        def img_getter(idx: int):
            img_tensor = self.forward(self.samples[idx].unsqueeze(0))[0]
            return to_pil_image(img_tensor)

        if user_data is None:
            user_data = [
                TooltipData(
                    name=f"Sample {ii}",
                    image_data=ii,
                    image_read_func=img_getter,
                )
                for ii in range(self.samples.shape[0])
            ]
        tformed = tensor_to_np(self.dim_tformer.transform(self.samples))
        cmap = fns.getAnyPgColormap(self.samples_info.colormap, forceExist=True)
        brushes = None
        if self.samples_info.numeric_labels is not None:
            brushes = cmap.map(self.samples_info.numeric_labels)
        self.scatterplot.setData(
            tformed[:, self.xdim],
            tformed[:, self.ydim],
            data=user_data,
            pxMode=False,
            size=size,
            brush=brushes,
        )
        self.legend.clear()
        if not show_legend:
            return
        for label, brush in self.brush_map.iteritems():
            self.legend.addItem(
                pg.ScatterPlotItem(symbol="o", brush=brush, pen="w"), label
            )
        self.legend.setVisible(show_legend)

    def set_transformer(
        self, transformer="PCA", n_components=None, max_n_samples=10_000
    ):
        samples = self.samples
        labels = self.numeric_labels
        if max_n_samples:
            rows = np.random.permutation(samples.shape[0])[:max_n_samples]
            samples = samples[rows]
            labels = labels[rows]

        tformer = transformer_classes[transformer](n_components=n_components)
        self.dim_tformer.set_tformer(tformer)
        self.dim_tformer.fit(samples, y=labels)
        self.update_dims_proc(xdim=0, ydim=1)


class VAEGenerativeTab(GANGenerativeTab):
    model: BaseVAE

    def forward(self, input):
        return self.model.decode(input)

    def get_num_latent_dims(self):
        return 128


class VAEEncoderTab(VAEGenerativeTab):
    encode_proc: AtomicProcess

    def __init__(self, model):
        self.encoded_roi = pg.CircleROI((0, 0), radius=0.1, pen="r", movable=False)
        super().__init__(model)

    def post_init(self, pe: ParamEditor):
        nested = NestedProcess.fromFunction(self.samples_from_folder)
        nested.allowDisable = False
        nested.addFunction(
            self.color_samples_from_file, file=dict(type="file", value="")
        )
        for stage in nested:
            stage.allowDisable = True
            stage.cacheOnEq = False
        pe.registerFunc(nested)

        pe.registerFunc(self.multiple_encode)

        super().post_init(pe)

    def on_tgt_move(self, tgt: pg.TargetItem):
        if self.samples is None or self.selected_preview_idx is None:
            return
        super().on_tgt_move(tgt)
        self.encode_selected_sample()

    def setup_gui(self):
        out = super().setup_gui()
        self.plot_widget.addItem(self.encoded_roi)

        while self.encoded_roi.handles:
            self.encoded_roi.removeHandle(0)
        return out

    def encode_selected_sample(self):
        nr, nc = self.num_preview_rows, self.num_preview_cols
        row, col = np.unravel_index(self.selected_preview_idx, (nr, nc))
        input_image = torch.tensor(self.preview_items[row][col].image, device=device)
        input_image = input_image.permute(2, 0, 1).unsqueeze(0)

        mu, std = self.forward_encoder(input_image)
        mu_xform = self.dim_tformer.transform(mu).view(-1)
        std_xform = self.dim_tformer.transform(std).view(-1)
        self.encoded_roi.setSize(tuple(tensor_to_np(std_xform[[self.xdim, self.ydim]])))
        self.encoded_roi.setPos(*tensor_to_np(mu_xform[[self.xdim, self.ydim]]))

        # if isinstance(img_or_path_or_sample, int):
        #     error = torch.abs(
        #         encoded - self.samples[img_or_path_or_sample].view(-1)
        #     ).sum()
        #     print(f"Encoding error: {error}")

    def multiple_encode(self):
        preview_idx = self.selected_preview_idx
        sample_idx = self.sample_idx_cache[preview_idx]
        user_data: TooltipData = self.scatterplot.data["data"][sample_idx]
        img_btyes = user_data.image_data_to_b64()
        if not isinstance(img_btyes, bytes):
            raise ValueError("Image must have valid data before encoding")
        img_bytes = base64.b64decode(img_btyes)
        image = Image.open(BytesIO(img_bytes))
        image_tensor = self.image_as_normed_tensor(image)

        layout = pg.GraphicsLayout()
        view = pg.GraphicsView()
        view.setCentralItem(layout)

        ref_vb = layout.addViewBox(row=0, col=0, lockAspect=True)
        ref_img = pg.ImageItem(np.array(image))
        ref_vb.addItem(ref_img)

        encoded_vb = layout.addViewBox(row=0, col=1, lockAspect=True)
        encoded_img = pg.ImageItem(ref_img.image)
        encoded_vb.addItem(encoded_img)
        err_plot = layout.addPlot(row=1, col=0, colspan=2)
        err_curve = pg.PlotCurveItem(pen="r", width=3)
        err_plot.addItem(err_curve)

        @functools.lru_cache(maxsize=30)
        def cached_encode(n=0):
            diffs = []
            if n <= 0:
                return diffs, image_tensor
            diffs, prev_image = cached_encode(n - 1)
            mu, _ = self.forward_encoder(prev_image)
            next_image = self.forward(mu)
            diffs = diffs.copy()
            diffs.append(torch.abs(next_image - prev_image).sum().item())
            return diffs, next_image

        def encode_decode(n=0):
            diffs, encoded_tensor = cached_encode(n)
            image_np = tensor_to_np(encoded_tensor[0]).transpose((1, 2, 0))
            encoded_img.setImage(image_np)
            err_curve.setData(np.arange(n), diffs)

        editor = ParamEditor()
        editor.registerFunc(
            encode_decode,
            runOpts=RunOpts.ON_CHANGED,
        )

        win = widgets.EasyWidget.buildWidget(
            [view, editor.dockContentsWidget], "H", useSplitter=True
        )
        self.popouts[f"encode_win{preview_idx}"] = win
        win.show()
        pg.exec()

    def samples_from_folder(
        self,
        source: str | Path | list | torch.Tensor = "",
        batch_size=32,
        std_scale=0.2,
        max_num_samples=1_000,
    ):
        """
        Generates latent samples by encoding each image in a folder.

        :param source: Folder of images. If blank, random latent samples are used instead
            type: file
            fileMode: Directory
        :param batch_size: Batch size to read images. Useful when dealing with large
            amounts of data.
        :param std_scale: Scale of the standard deviation to use for the PCA so
            all samples are visible
        :param max_num_samples: Upper bound on randomly selected samples from the
            source.
        """
        if not os.path.exists(source):
            return
        samples, names = read_image_folder(source)
        if not samples:
            return
        if max_num_samples < len(samples):
            keep_idxs = np.random.choice(len(samples), max_num_samples, replace=False)
            samples = [samples[i] for i in keep_idxs]
            names = [names[i] for i in keep_idxs]
        mu_all, std_all = [], []
        for batch_start in range(0, len(samples), batch_size):
            batch = samples[batch_start : batch_start + batch_size]
            batch = torch.cat([self.image_as_normed_tensor(im) for im in batch], dim=0)
            mu, std = self.forward_encoder(batch)
            mu = mu.view(-1, mu.shape[1])
            std = std.view(-1, std.shape[1])
            mu_all.append(mu)
            std_all.append(std)
        mu_all = torch.cat(mu_all, dim=0)
        std_all = tensor_to_np(torch.cat(std_all, dim=0))

        self.samples_info.unset_labels()
        self.samples_info.samples = mu_all
        user_data = [
            TooltipData(name=name, image_data=os.path.join(source, name))
            for ii, name in enumerate(names)
        ]
        size = std_all[:, [self.xdim, self.ydim]].mean(axis=1) * std_scale
        self.plot_samples(size=size, user_data=user_data)
        self.sample_idx_cache = np.random.choice(
            range(len(mu_all)), len(self.sample_idx_cache), replace=False
        ).tolist()
        self.update_previews()

    def forward_encoder(self, input):
        mu, log_var = self.model.encode(input)
        std = torch.exp(0.5 * log_var)
        return mu, std

    def color_samples_from_file(
        self, file, label_col="", colormap="plasma", legend=False
    ):
        """
        :param file: Csv with at least ``label_col`` present
        :param label_col: Column in ``file`` to use as label
        :param colormap: Colormap to use for drawing the scatterplot
        :param legend: Whether to show a legend of all unique labels
            ignore: True
        """
        df = pd.read_csv(file, dtype=str, na_filter=False)
        if len(df) != len(self.scatterplot.getData()[0]) or label_col not in df:
            return
        labels = df[label_col]
        param = PrjParam(label_col, "")
        numeric, mapping = param.toNumeric(labels, rescale=True, returnMapping=True)
        self.samples_info.colormap = colormap
        self.samples_info.number_to_label_map = mapping
        self.samples_info.numeric_labels = numeric

        self.plot_samples(user_data=self.scatterplot.data["data"], show_legend=legend)


class GANDiscriminitiveTab(ModelTab):

    preview_items: List[List[pg.ImageItem]]
    preview_widget: pg.GraphicsView

    def setup_gui(self):
        return widgets.EasyWidget.buildWidget(
            [self.preview_widget, self.editor.dockContentsWidget],
            layout="H",
            useSplitter=True,
        )

    def post_init(self, pe: ParamEditor):
        pe.registerFunc(self.predict_on_samples)

    def predict_on_samples(self, image_or_path=""):
        """
        :param image_or_path:
            type: file
            fileMode: Directory
        """
        if isinstance(image_or_path, np.ndarray):
            images = [Image.fromarray(image_or_path)]
            names = ["Confidence"]
        elif isinstance(image_or_path, (str, Path)):
            images, names = read_image_folder(image_or_path)
        else:
            raise ValueError(f"Invalid image or path type: {type(image_or_path)}")

        img_as_batch = torch.cat(
            [self.image_as_normed_tensor(image) for image in images]
        )
        confidences = (
            self.model.discriminator(img_as_batch)
            .view(-1)
            .detach()
            .cpu()
            .numpy()
            .round(2)
        )
        num_rows = int(np.sqrt(len(images)))
        num_cols = int(np.ceil(len(images) / num_rows))
        images_np = [tensor_to_np(im).transpose((1, 2, 0)) for im in img_as_batch]
        self.make_previews(num_rows, num_cols)
        self.update_previews(images_np, confidences, names, show_labels=True)

    get_num_latent_dims = GANGenerativeTab.get_num_latent_dims
    get_image_size = GANGenerativeTab.get_image_size

    def forward(self, input):
        return self.model.discriminate(input)


def dummy_tab_cls(*_args):
    return None


class ModelWindow(QtWidgets.QMainWindow):
    sig_model_changed = QtCore.Signal()

    tab_types: List[Type[ModelTab]] = []
    model_cls = None

    def __init__(self, model_param_info=None):
        super().__init__()
        self.model = self.make_model(model_param_info)
        self.model.to(device).eval()
        self.settings_editor = ParamEditor()

        self.setup_gui()

        pe = self.settings_editor
        self.update_weights_proc = pe.registerFunc(self.update_weights)
        pe.registerFunc(self.dev_console)
        self.update_weights()

    def update_weights(
        self,
        weights="",
    ):
        """
        :param weights:
            type: file
        """
        if not Path(weights or "").is_file():
            return
        data = torch.load(weights, map_location=device)
        result = load_trainer_state_dict(self.model, data, strict=False)

        if not any(result):
            msg = str(result)
        else:
            missing, unexpected = [self._format_result_message(keys) for keys in result]
            msg = f"Missing keys: {missing}"
            msg += ", Unexpected keys: " + f"{unexpected}"
        self.statusBar().showMessage(msg)
        self.sig_model_changed.emit()

    @staticmethod
    def _format_result_message(key_list):
        if not key_list:
            return "<None>"
        missing_networks = set(key.split(".")[0] + ".*" for key in key_list)
        return ", ".join(missing_networks)

    # Variables are widget logistics, not used elsewhere
    # noinspection PyAttributeOutsideInit
    def setup_gui(self):
        self.tab_group = QtWidgets.QTabWidget()
        for tab_cls in self.tab_types:
            self.insert_tab(tab_cls)

        mb = self.menuBar()
        self.settings_widget = widgets.EasyWidget.buildWidget(
            [self.settings_editor.tree]
        )
        mb.addAction("&Settings", self.raise_settings_window)

        widgets.EasyWidget.buildMainWin([self.tab_group], win=self)

    def raise_settings_window(self):
        self.settings_widget.show()
        self.settings_widget.raise_()

    def insert_tab(self, tab_type: Type[ModelTab]):
        tab_inst = tab_type(self.model)
        tab_name = "&" + tab_inst.__class__.__name__.replace("Tab", "")
        self.tab_group.addTab(tab_inst, tab_name)
        self.sig_model_changed.connect(tab_inst.on_model_change)

    def dev_console(self):
        widgets.safeSpawnDevConsole(win=self)

    @classmethod
    def make_model(cls, param_info=None):
        if cls.model_cls is None:
            raise ValueError("Model class not set")

        if param_info is None:
            param_info = {}
        elif isinstance(param_info, str):
            param_info = get_config(param_info)
            # Came from config file, parse portion of file that has params
            # for the model class
            model_cls_name = cls.model_cls.__name__.lower()
            param_info = param_info[f"{model_cls_name[-3:]}_model_params"]

        return cls.model_cls(**param_info)


class GANWindow(ModelWindow):
    tab_types = [GANGenerativeTab, GANDiscriminitiveTab]
    model_cls = GAN


class VAEWindow(ModelWindow):
    tab_types = [VAEEncoderTab]
    model_cls = DFCVAE


class MultiModel:
    def __init__(self, **named_models):
        self.named_models = named_models

        for method in "to", "load_state_dict", "eval", "train":
            func = functools.partial(self.call_with_models, method)
            setattr(self, method, func)

    def call_with_models(self, method, *args, **kwargs):
        for model in self.named_models.values():
            getattr(model, method)(*args, **kwargs)
        return self


class MultiModelWindow(ModelWindow):
    tab_types = [VAEEncoderTab, GANGenerativeTab, GANDiscriminitiveTab]
    model_cls = None
    model: MultiModel

    @classmethod
    def make_model(cls, param_info=None) -> MultiModel:
        if isinstance(param_info, str):
            param_info = get_config(param_info)
        out_models = {}
        # Need to load param info for both model types
        for win_type in GANWindow, VAEWindow:
            win_name = win_type.__name__.lower()[:3]
            name_key = f"{win_name}_model_params"
            if param_info and name_key in param_info:
                model_param_info = param_info[name_key]
            else:
                model_param_info = {}
            out_models[win_name] = win_type.model_cls(**model_param_info)
        return MultiModel(**out_models)

    def update_weights(
        self,
        weights="",
        model_name="gan",
    ):
        """
        :param weights:
            type: file
        :param model_name:
            type: list
            limits: ["gan", "vae"]
        """
        old_model: MultiModel = self.model

        if model_name not in old_model.named_models:
            raise ValueError(
                f"Invalid model name: {model_name}. Must be one of: {list(old_model.named_models)}"
            )

        model_to_update = old_model.named_models[model_name]

        try:
            self.model = model_to_update
            super().update_weights(weights)
        finally:
            self.model = old_model

    def insert_tab(self, tab_type: Type[ModelTab]):
        tab_name = tab_type.__name__.lower()
        tab_inst = None
        for model_name, model_inst in self.model.named_models.items():
            if tab_name.startswith(model_name):
                tab_inst = tab_type(model_inst)
                break
        if not tab_inst:
            raise ValueError(f"Invalid tab type: {tab_type}")
        tab_name = "&" + tab_inst.__class__.__name__.replace("Tab", "")
        self.tab_group.addTab(tab_inst, tab_name)


def main(
    viz_type="multi",
    model_file="",
    param_file="model_defaults.yaml",
    **kwargs,
):
    pg.mkQApp()
    window_type_map = {
        "gan": GANWindow,
        "vae": VAEWindow,
        "multi": MultiModelWindow,
    }
    if viz_type not in window_type_map:
        raise ValueError(
            f"Invalid viz_type: {viz_type}. Must be one of: {window_type_map}"
        )
    win = window_type_map[viz_type](param_file)
    if model_file:
        win.update_weights_proc(weights=model_file, **kwargs)

    QtCore.QTimer.singleShot(0, win.showMaximized)
    pg.exec()


if __name__ == "__main__":
    cli = fns.makeCli(main)
    main(**vars(cli.parse_args()))
