import os
import random

import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks import (
    LearningRateMonitor,
    ModelCheckpoint,
    EarlyStopping,
)
from pytorch_lightning.loggers import TensorBoardLogger

from ...common import constants
from ...common.dataset import LitGMVLoader
from .trainer import GANTrainerState

# Set random seed for reproducibility
manualSeed = 999
# manualSeed = random.randint(1, 10000) # use if you want new results
# print("Random Seed: ", manualSeed)
random.seed(manualSeed)
torch.manual_seed(manualSeed)


def main(
    dataroot="datagen/Png Export/images",
    log_dir="lightning_logs",
    workers=0,
    batch_size=16,
    image_size=64,
    num_epochs=50,
    ngpu=1,
    pad_on_resize=True,
    ckpt_path=None,
    **gan_kwargs
):
    """
    :param dataroot: Root directory for dataset
    :param log_dir: Where to save model checkpoints, etc.
    :param workers: Number of workers for dataloader
    :param batch_size: Size of training batch
    :param image_size: Square image side length in pixels, i.e. each image is (size, size, channels)
    :param num_epochs: Max training epochs
    :param ngpu: Number of training GPUs, set to 0 for CPU
    :param pad_on_resize: Whether to pad when resizing to maintain aspect ratio
    :param ckpt_path: Path to checkpoint to load
    """
    if ngpu is None:
        ngpu = constants.num_gpus
    to_save = locals()

    tb_logger = TensorBoardLogger(save_dir=log_dir, name="DCGAN")
    tb_logger.log_hyperparams(to_save)

    trainable_state = GANTrainerState(**gan_kwargs)
    trainer_engine = pl.Trainer(
        logger=tb_logger,
        max_epochs=num_epochs,
        log_every_n_steps=2,
        callbacks=[
            LearningRateMonitor(),
            ModelCheckpoint(
                save_top_k=num_epochs,
                dirpath=os.path.join(tb_logger.log_dir, "checkpoints"),
                monitor="val_loss",
                save_last=True,
                every_n_epochs=1,
            ),
            EarlyStopping(monitor="val_loss", patience=15),
        ],
        gpus=list(range(ngpu)),
    )

    patch_size = (image_size, image_size)
    loader = LitGMVLoader(
        dataroot,
        batch_size,
        batch_size,
        patch_size,
        workers,
        pad_on_resize=pad_on_resize,
    )
    trainer_engine.fit(trainable_state, loader, ckpt_path=ckpt_path)
