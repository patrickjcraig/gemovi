import argparse
from typing import Literal

from ..common.utils import get_config


def main(
    framework: Literal["ignite", "lightning"],
    config_file="model_defaults.yaml",
    **kwargs,
):
    config = get_config(config_file)
    data = config["data_params"]
    model = config["gan_model_params"]
    trainer = config["trainer_params"]
    defaults = dict(
        dataroot=data["data_path"],
        log_dir=trainer["log_dir"].format(framework=framework),
        workers=data["num_workers"],
        image_size=data["patch_size"],
        batch_size=config["data_params"]["train_batch_size"],
        num_epochs=trainer["max_epochs"],
        ngpu=len(trainer["gpus"]),
        ckpt_path=trainer["ckpt_path"],
        **model,
    )
    defaults.update(kwargs)

    if framework == "ignite":
        # False positive triggered here
        from .train_ignite import main as inner_main  # unimport: skip
    elif framework == "lightning":
        from .train_lightning import main as inner_main
    else:
        raise ValueError(f"Unknown framework: {framework}")
    inner_main(**defaults)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generic runner for GAN models")
    parser.add_argument(
        "--framework",
        "-f",
        help="trainer framework, either ignite or lightning",
    )
    parser.add_argument(
        "--config",
        "-c",
        help="path to the config file",
        default="model_defaults.yaml",
    )
    args = parser.parse_args()
    main(args.framework, args.config)
