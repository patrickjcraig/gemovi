import torch
from ignite import distributed as idist

from ...common.utils import tensor_keys_to_items
from ..models import GAN


class IgniteGan(GAN):
    manage_gradients = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, model in self.named_children():
            setattr(self, name, idist.auto_model(model))

        for name in "gen_optim", "discrim_optim", "encoder_optim":
            if cur_optim := getattr(self, name):
                setattr(self, name, idist.auto_optim(cur_optim))

    def train_step_ignite(self, engine, batch):
        device = idist.device()
        batch, noise = self.resolve_batch_and_noise(batch)
        batch, noise = batch.to(device), noise.to(device)

        log = dict()
        for ii, _ in enumerate(self.children()):
            next_log = self.train_step_for_idx(ii)(batch, noise)
            log.update(next_log)
        log = tensor_keys_to_items(log)
        # Sort so "err_*" keys are first
        log = sorted(log.items(), key=lambda x: (not x[0].startswith("err_"), x[0]))
        log = dict(log)
        return log

    def get_log_keys(self):
        im_size = self.opts.image_size
        old_manage_gradients = self.manage_gradients
        if isinstance(im_size, int):
            im_size = (im_size, im_size)
        try:
            with torch.no_grad():
                self.manage_gradients = False
                batch = torch.zeros(1, self.opts.num_image_channels, *im_size)
                return list(self.train_step_ignite(None, batch))
        finally:
            self.manage_gradients = old_manage_gradients
