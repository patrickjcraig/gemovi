import os
import random
import re
from pathlib import Path

import pandas as pd
import torch
import yaml
from ignite.contrib.handlers import ProgressBar
from ignite.engine import Engine, Events
from ignite.handlers import EarlyStopping, ModelCheckpoint
from torch.utils.data import DataLoader

from ...common import constants
from ...common.dataset import GMVDataset, default_image_transforms
from ...common.utils import load_trainer_state_dict
from .trainer import IgniteGan

# Set random seed for reproducibility
manualSeed = 999
# manualSeed = random.randint(1, 10000) # use if you want new results
# print("Random Seed: ", manualSeed)
random.seed(manualSeed)
torch.manual_seed(manualSeed)


def main(
    dataroot: str,
    log_dir="ignite_logs",
    checkpoint_version_fmt="v{num}",
    workers=0,
    batch_size=16,
    image_size=64,
    num_epochs=50,
    ngpu=None,
    pad_on_resize=None,
    ckpt_path=None,
    **gan_kwargs,
):
    """
    :param dataroot: Root directory for dataset
    :param log_dir: Where to save model checkpoints, etc.
    :param checkpoint_version_fmt: Format string for checkpoint version
    :param workers: Number of workers for dataloader
    :param batch_size: Size of training batch
    :param image_size: Square image side length in pixels, i.e. each image is (size, size, channels)
    :param num_epochs: Max training epochs
    :param ngpu: Number of training GPUs, set to 0 for CPU
    :param pad_on_resize: Whether to pad when resizing to maintain aspect ratio
    :param ckpt_path: Path to checkpoint to load
    """
    if ngpu is None:
        ngpu = constants.num_gpus
    to_save = locals()

    gan = IgniteGan(
        **gan_kwargs,
    )

    if ckpt_path is not None:
        load_trainer_state_dict(gan, torch.load(ckpt_path))

    trainer_engine = Engine(gan.train_step_ignite)
    pbar = ProgressBar()
    pbar.attach(trainer_engine, metric_names="all")

    output_record_df = pd.DataFrame(columns=gan.get_log_keys())
    longest_key = max(len(key) for key in output_record_df.columns)
    epoch_repr_len = len(str(num_epochs))
    float_format = f"{{:>{longest_key+2}.4f}}".format
    index_format = f"{{:<{epoch_repr_len+1}d}}".format

    @trainer_engine.on(Events.EPOCH_COMPLETED)
    def print_and_save_record(engine: Engine):
        idx = engine.state.epoch
        output_record_df.loc[idx] = engine.state.output
        print(
            output_record_df.loc[[idx]].to_string(
                header=idx == 1,
                float_format=float_format,
                formatters={"__index__": index_format},
            )
        )
        if log_dir is not None:
            out_path = os.path.join(log_dir, "log.csv")
            output_record_df.to_csv(
                out_path, index_label="epoch", float_format="{:.4f}".format
            )

    def score_function(engine):
        val_loss = engine.state.output["err_g"] + engine.state.output["err_d"]
        return -val_loss

    handler = EarlyStopping(
        patience=15, score_function=score_function, trainer=trainer_engine
    )
    trainer_engine.add_event_handler(Events.EPOCH_COMPLETED, handler)

    existing_ckpts = list(
        Path(log_dir).glob(checkpoint_version_fmt.replace("{num}", "*"))
    )

    if not existing_ckpts:
        max_num = 0
    else:
        max_num = max(int(re.search(r"\d+", p.name).group()) for p in existing_ckpts)
    log_dir = os.path.join(log_dir, checkpoint_version_fmt.format(num=max_num + 1))
    cp = ModelCheckpoint(log_dir, "gan", n_saved=None)
    trainer_engine.add_event_handler(Events.EPOCH_COMPLETED, cp, dict(model=gan))
    state_file = Path(log_dir) / "train_state.yaml"
    state_file.parent.mkdir(exist_ok=True)
    with open(state_file, "w") as ofile:
        yaml.dump(to_save, ofile)

    dataset = GMVDataset(
        dataroot,
        folder_is_class=True,
        transform=default_image_transforms(image_size, pad_on_resize),
    )
    loader = DataLoader(dataset, batch_size, num_workers=workers)
    trainer_engine.run(loader, max_epochs=num_epochs)
